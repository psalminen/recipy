#!/usr/bin/env python3
# -*- coding=utf-8 -*- 
""" Find recipes based on ingredients
This is using spoonacular which does a lot of the heavy lifting
"""

import tabulate

import spoonpy

def get_items():
    """ Get the items from user input
    Enter one item per line, ending with a blank line
    This is to be used when run from a command line/IDLE

    Returns:
        List of items given
    """
    print("Enter one item per line. Leave blank when finished.")
    return list(iter(input, ''))


def clean_matches(data):
    """ Exctract the information we want to display from each recipe result

    Args:
        data: The JSON results we got
    Returns:
        A list of lists with cleaned data
    """
    display_info = []
    sorted_data = sorted(data, key=lambda x: x['missedIngredientCount'])
    for entry in sorted_data:
        # This is called list comprehension, super useful
        missing_ingredients = [i['name'] for i in entry['missedIngredients']]
        entry_info = [
            entry['id'], 
            entry['title'], 
            ', '.join(missing_ingredients)
        ]
        display_info.append(entry_info)

    return display_info


def choose_match(data):
    """ Display the matched recipes we got, and get a selection from the user

    Args:
        data: The JSON results we got
    Returns:
        The ID of the selected recipe
    """
    matches = clean_matches(data)
    headers=("ID", "Recipe", "Missing Ingredients")

    # Check to make sure the user entered a valid ID (a number)
    # This will keep printing the matches and asking for an entry
    # Until a number is enterred
    while True:
        print(tabulate.tabulate(matches, headers=headers))
        choice = input('Enter desired Recipe ID: ')
        try:
            return int(choice)
        except ValueError as error:
            print(f'\n ERROR: {error}\n')


def clean_ingredients(ingredients):
    """ Clean the ingredients response into something readable
    This is just parsing through the JSON (dict) to get a human-readable result. Its a very common thing youll have to do.

    Args:
        ingredients: The raw JSON response
    Returns:
        A list of "*INGREDIENT* - *Amount* *Unit*"
    """
    ingredient_strings = []
    for ingredient in ingredients:
        # This is the same as ing['name']
        # Except that if 'name' isnt a key, the value will be None
        name = ingredient.get('name')
        amt = ingredient.get('amount')
        unit = ingredient.get('unit')

        # Make sure all the keys exist
        if name and amt and unit:
            ing_str = f"{name} - {amt} {unit}"
        else:    # Otherwise use the originalString
            ing_str = ingredient.get('originalString', 'MISSING')

        ingredient_strings.append(ing_str)

    return ingredient_strings


def display_recipe(recipe):
    """ Similar to `display_matches`, except it doesnt return anything. Instead, it just shows some basic stuff about the chosen recipe

    Args:
        recipe: Json response about the recipe (a dictionary)
    """
    ingredients = clean_ingredients(recipe['extendedIngredients'])
    ingredients_str = '\n\t'.join(ingredients)

    # I use f-strings a lot throughout, but notice
    # how I need double quotes for the string 
    # so I can use the single quotes inside of it
    print("\n\n------------------------------------------")
    print(f"Name: {recipe['title']}")
    print("------------------------------------------")
    print(f"Summary: {recipe['summary']}")
    print("------------------------------------------")
    print(f"Ingredients:\n\t{ingredients_str}")
    print("------------------------------------------")
    print(f"Instructions: {recipe['instructions']}")


# This is what happens when you start the program
# Since calling the file directly makes the "name" == "main"
if __name__ == '__main__':
    items = get_items()
    recipes = spoonpy.get_recipes_from_ingredients(items)
    choice = choose_match(recipes)
    chosen_recipe = spoonpy.get_recipe(choice)
    display_recipe(chosen_recipe)

