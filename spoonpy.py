#!/usr/bin/env python3
# -*- coding=utf-8 -*- 
""" Spoonacular API SDK
Functions for communicating with the Spoonacular API

For more information about their API go to https://spoonacular.com/food-api/docs
You will need to get a (free) account and insert the API key below
"""

# This is a non-standard library import
# To use it you install it with pip
# Pip's a command line tool run like so,
# pip install requests
# I also put it in requirements.txt, so you can run
# pip install -r requirements.txt
# All required packages
import requests

import config
# Insert your spoonacular API key here
# To get one, sign up for an account
# https://spoonacular.com/food-api
# Also, the CAPS here is good practice for global variables

# Set global variables for API URI
# If you arent familiar with f-strings, checkem out
# They allow you to insert variables into strings
# by using a bracket (see the {BASE_URI} part)
BASE_URI = "https://api.spoonacular.com"
RECIPE_URI = f"{BASE_URI}/recipes"


class BadResponseError(Exception):
    """ Error to raise if the API call does not work"""
    pass


def get_recipes_from_ingredients(items, number=10, ranking=2, ignore_pantry=True):
    """ Make the API call to get all the recipes
    https://spoonacular.com/food-api/docs#Search-Recipes-by-Ingredients

    Args:
        items: List of items to find recipes for
        number: Number of recipes to get
        ranking: 1 = maximize used ingredients
                 2 = minimize missing ingredients
    Returns:
        List of dictionaries describing the recipes
    """
    # Define the url we want to use
    uri = f"{RECIPE_URI}/findByIngredients"

    # Define the body of information we will be
    # sending to to API to define what information
    # we want. This is almost always a dictionary
    # The keys I use here are defined by the API
    # in the link above
    body = {
        "apiKey": config.API_KEY,
        "ingredients": ','.join(items),
        "number": number,
        "ranking": ranking,
        "ignorePantry": ignore_pantry,
    }

    # Make the request and get the response
    response = requests.get(uri, params=body)
    if response.status_code != 200:    # 200 is the success code
        raise BadResponseError(response.content)

    # JSON is essentially just lists and dictionaries.
    # This is the information we actually want
    # from the response
    return response.json()


def get_recipe(id_, include_nutrition=False):
    """ Get information on a selected recipe
    https://spoonacular.com/food-api/docs#Get-Recipe-Information

    Args:
        id_: ID of selected recipe
        include_nutrition: Whether to include nutrition data on the recipe.
    Returns:
        Dictionary of information about the recipe
    """
    # I use id_ instead of id because id is a python
    # Keyword. Standard practice is to use an underscore
    # after a keyword if you want to 
    # use it as a variable name
    uri = f"{RECIPE_URI}/{id_}/information"

    # Define the body of information to send the API
    body = {
        "apiKey": config.API_KEY,
        "includeNutrition": include_nutrition,
    }

    # Make the request and return the response
    response = requests.get(uri, params=body)
    if response.status_code != 200:
        raise BadResponseError(response.content)
    return response.json()

